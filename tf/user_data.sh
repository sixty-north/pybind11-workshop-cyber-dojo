#!/bin/bash

/home/ubuntu/commander/cyber-dojo start-point rm python-pytest
/home/ubuntu/commander/cyber-dojo start-point rm pybind11-workshop

/home/ubuntu/commander/cyber-dojo start-point create python-pytest --git=https://github.com/cyber-dojo-languages/python-pytest
/home/ubuntu/commander/cyber-dojo start-point create pybind11-workshop --git=http://bitbucket.org/sixty-north/pybind11-workshop-cyber-dojo.git

/home/ubuntu/commander/cyber-dojo up --languages=python-pytest --custom=pybind11-workshop
