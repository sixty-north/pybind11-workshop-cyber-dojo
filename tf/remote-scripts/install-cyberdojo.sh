#/bin/bash
set -e

# curl -O https://raw.githubusercontent.com/cyber-dojo/commander/master/cyber-dojo
# chmod +x cyber-dojo

REPOS="collector \
       commander \
       cyber-dojo \
       differ \
       grafana \
       nginx \
       prometheus \
       repl_container_python \
       runner_processful \
       runner_repl \
       runner_stateful \
       runner_stateless \
       starter \
       storer \
       web \
       zipper"

for REPO in $REPOS; do
    git clone -b python-repl https://github.com/abingham/$REPO
done

sudo cyber-dojo/dev/build-all.sh
