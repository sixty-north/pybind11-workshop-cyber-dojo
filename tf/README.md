# Terraform your own cyber-dojo on AWS

## Step 0 - Finding your AWS access/secret keys

If you don't have this information saved somewhere, then you'll probably need to
regenerate it.

To do this, go to the "IAM" service in AWS. Then select "Users" from the
left-hand menu. Click on the user (probably "cyberdojo-tf-user") from the users,
and then select the "Security credentials" tab. Now you'll see the currently
configured access keys, and you can create a new one if you need to (as well as
delete old ones).

## Step 1 - Create the AMI for your instance

This step is only necessary if you don't have an image already or if you need to
update the image. You would need to update the image if, for example, you change
the workshop exercises.

    packer build -var 'aws_secret_key=<secret key>' -var 'aws_access_key=<access key>' packer.json

Note that you might want to remove old AMIs when you do this.

### When to update the AMI

You need to update the AMI when we change a) the base image or b) the stuff we
install on it. You'll have to look in `packer.json` to know this information
definitively. At the time of writing, though, the things we put in the image are
the scripts in `remote-scripts`; if you change those, you need to re-run
`packer`.

## Step 2 - Get your aws keys

Download your private key and put it in a folder ssh/mykey.pem

The key pairs we need for this are in the EC2 section of AWS. From the
"Services" menu go to "EC2". Then select "Key pairs" from the left-hand menu.
Unless you already have the public/private pair for "cyber-dojo-key", you'll
have to delete that pair from AWS, generate new ones, and upload them. Use
ssh-keygen for this.

## Step 3 - create a `terraform.tfvars` file

Copy `terraform.tfvars.TEMPLATE` to `terraform.tfvars` and edit as appropriate.

## Step 4 - run the terraform

    terraform plan
    terraform apply

At this point the cyber dojo should be up and running with the pybind11 workshop
available. You still need to manually update the DNS on mythic-beasts if you
want cyberdojo.sixty-north.com to work.

### SSH access to the machine

If the `terraform apply` works, you'll have an EC2 instance running. If you need
to SSH into it for some reason, find it's public IP from AWS. Then run `ssh -i
ssh/mykey.pem ubuntu@<public ip address>`.

## Step 5 - destroy your server

    terraform destroy
